import React from 'react';
import './Task.css';

const task = (props) => {
  return ( 
    <div className="Task">
      <p onClick={ props.click } >
        The task "{ props.title }" status is { props.status } 
      </p>
      { props.children ? <p> { props.children } </p>  : null }
      <input type="text" onChange = { props.changed } value = { props.status }/>
    </div>
  )
}

export default task;
