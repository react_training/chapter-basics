import React, { Component } from 'react';
import './App.css';
import Task from './Task/Task';

class App extends Component {
  
  state = {
    tasks: [
      { id: '18928', title: 'Learn React', status: 'open', detail: 'Followed by : Mohamed Hamidi'},
      { id: '08179', title: 'Learn React Native', status: 'in progress', detail: ''},
      { id: '65341', title: 'Master Angular', status: 'archived', detail: ''}
    ],
    toggleTasksBoolean: true
  }

  displayTasks = () => {
    return (
      // this.state.toggleTasksBoolean ? 
      // <Fragment>
      //   <Task title={ this.state.tasks[0].title } status={ this.state.tasks[0].status } >
      //     Followed by : <strong>Mohamed Hamidi</strong>
      //   </Task>
      //   <Task title={ this.state.tasks[1].title } 
      //         status={ this.state.tasks[1].status } 
      //         changed = { this.updateTaskStatusHandler }
      //         click={ () => this.changeTaskStatusHandler('closed') }/>
      //   <Task title={ this.state.tasks[2].title } 
      //         status={ this.state.tasks[2].status } />
      // </Fragment> : null

      this.state.toggleTasksBoolean && 
      this.state.tasks.map(task => {
          return ( <Task key={ task.id } title={ task.title } 
                        status={ task.status } 
                        click= { this.deleteTaskhandler.bind(this, task.id) } 
                        changed = { (event) => this.updateTaskStatusHandler(event, task.id) } >
                        { task.detail } 
                    </Task>)
      } )
    )
  }

  changeTaskStatusHandler = (newStatus) => {
    this.setState({
      tasks: [
        { title: 'Dive Deep into React', status: newStatus},
        { title: 'Learn React Native', status: 'in progress'},
        { title: 'Master Angular', status: 'archived'}
      ]
    });
  }

  updateTaskStatusHandler = (event, id) => {
    const tasks = this.state.tasks.map(task => {
      if(task.id === id) {
        task.status = event.target.value;
      }
      return task;
    })
    this.setState({
      tasks
    });
  }

  toggleTasksHandler = () => {
    this.setState({
      toggleTasksBoolean: !this.state.toggleTasksBoolean
    })
  }

  deleteTaskhandler = (id) => {
    // DO NOT UPDATE STATE DIRECTLY
    // const tasks = this.state.tasks;
    // const taskIndex = tasks.findIndex(task => task.id === id)
    // tasks.splice(taskIndex, 1);
    
    // UPDATING STATE IMMUTABLY
    // const tasks = [...this.state.tasks];
    // const taskIndex = tasks.findIndex(task => task.id === id)
    // tasks.splice(taskIndex, 1);

    // OR USE filter function which create a new array
    const tasks = this.state.tasks.filter((task) => task.id !== id);
    this.setState({
      tasks : tasks
    })
  }

  render() {
    // const btnStyle = {
    //   fontFamily: '"Ubuntu Mono"'
    // };
    
    return(
      <div className="App">
        <h1>Welcome everybody to this React course !!</h1>
        {/* when onClick called method has parenthesis, its code is executed when the page is loaded  */
        /* <button className="btn btn-primary" onClick={ this.changeTaskStatusHandler() } >Change Status</button> 
        <button style={ btnStyle } className="btn btn-primary" onClick={ this.changeTaskStatusHandler.bind(this, 'postponed') } >Change Status</button> */}
        <button className="btn btn-primary" onClick={ this.toggleTasksHandler } >Toggle Tasks</button>
        <hr/>
        { this.displayTasks() }
      </div>
    )
  }
}

export default App;
